default:
	@just --list

build-app:
	docker build -t rust-app .

run-app:
	docker run -d -p 8080:8080 rust-app

stop-app:
	#!/usr/bin/zsh
	docker stop "$(docker ps --quiet --filter ancestor=rust-app)"

build-db:
	#!/usr/bin/zsh
	cd docker-entrypoint-initdb.d
	docker build -t db .

run-db:
	docker run -i -p 5433:5432 -e POSTGRES_PASSWORD=secret -d db

stop-db:
	#!/usr/bin/zsh
	docker stop "$(docker ps --quiet --filter ancestor=db)"

stop-last-container:
	#!/usr/bin/zsh
	docker stop "$(docker ps --quiet)"

run-test-db:
	docker run --name test-db -p 5433:5432 -v /home/blanktiger/sem-6/io/lab/containers/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d -e POSTGRES_PASSWORD=secret -d postgres

rm-test-db-container:
	docker rm test-db

connect-to-test-db:
	psql -h localhost -p 5433 -U postgres

compose-build:
	docker-compose build

compose-run:
	docker-compose up

compose-stop:
	docker-compose down

gen-docs:
	cd docs/ && . ./venv/bin/activate && make clean && make html

gen-docs-on-change:
	cargo watch -c -w docs/source -- just gen-docs

install-py-req:
	cd docs/ && . ./venv/bin/activate && pip install -r requirements.txt

FROM rust:latest as builder
WORKDIR /app
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
COPY ./.env ./.env
RUN apt-get update & apt-get install -y extra-runtime-dependencies & rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/containers /usr/local/bin/containers
CMD ["containers"]

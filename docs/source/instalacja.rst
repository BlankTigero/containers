===========================================
Instalacja i uruchomienie środowiska docker
===========================================


----------
Instalacja
----------

Do uruchomienia aplikacji potrzebny jest zainstalowany docker oraz docker-compose. Podane poniżej instrukcje dotyczą instalacji na systemie Ubuntu 22.04.

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Zależności dockera i docker-compose
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Aby zainstalować należy wykonać poniższe polecenie:

.. code-block:: console

   $ sudo apt install curl gnupg ca-certificates lsb-release

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ustawienie repozytorium dockera
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Do instalacji samego dockera należy dodać repozytorium dockera. Aby to zrobić należy dodać klucz GPG dockera oraz dodać repozytorium dockera do listy repozytoriów systemu:

.. code-block:: console

   $ sudo mkdir -p /etc/apt/keyrings/
   $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
   $ echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-pluginsudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-pluginlinux/ubuntu   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Instalacja docker i docker-compose
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Wreszcie należy już tylko odświeżyć listę dostępnych paczek i zainstalować docker oraz docker-compose:

.. code-block:: console

   $ sudo apt update
   $ sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ułatwienie współpracy z dockerem
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sporym uproszczeniem w pracy z dockerem jest dodanie użytkownika do grupy docker. Dzięki temu nie trzeba za każdym razem używać sudo przy wykonywaniu poleceń związanych z dockerem. Aby to zrobić należy wykonać poniższe polecenia:

.. code-block:: console

   $ sudo groupadd docker
   $ sudo usermod -aG docker $USER
   $ newgrp docker

Następnie należy się wylogować i zalogować i sprawdzić czy docker działa poprawnie (czasami należy zrestartować system):

.. code-block:: console

   $ docker run hello-world

------------
Uruchomienie
------------

Uruchomienie aplikacji wraz z bazą danych odbywa się za pomocą polecenia:

.. code-block:: console

   $ docker-compose build
   $ docker-compose up -d

Po uruchomieniu można wykonywać zapytania do API. Aby zatrzymać aplikację należy wykonać polecenie:

.. code-block:: console

   $ docker-compose down

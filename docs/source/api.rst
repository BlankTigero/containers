==================
REST api endpoints
==================

---------------------
GET /api/customer/:id
---------------------

Dostarcza obiekt customer w postaci JSON-a.

---------------------
GET /api/customer/all
---------------------

Dostarcza obiekty wszystkich customerów w postaci JSON-owej listy.

.. containers-docs documentation master file, created by
   sphinx-quickstart on Tue May  2 19:20:45 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Docker/docker-compose docs
===========================================

.. toctree::
   :maxdepth: 2

   wprowadzenie
   instalacja
   architektura
   api

.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

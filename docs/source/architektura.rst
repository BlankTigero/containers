============
Architektura
============

------------
Diagram klas
------------

.. image:: class_diagram.svg


-------------------------
Diagram przypadków użycia
-------------------------

.. uml::

    @startuml
    skinparam actorStyle awesome
    :User: --> (Use)
    "Uzyskaj informacje o customerze po id" as (Use)
    :User: --> (Use2)
    "Uzyskaj informacje o wszystkich customerach" as (Use2)
    @enduml

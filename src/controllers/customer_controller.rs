use crate::app::DbPool;
use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::IntoResponse,
    Json,
};
use tracing::warn;

use crate::services::CustomerService;

pub struct CustomerController;

impl CustomerController {
    pub async fn get_customer(
        State(pool): State<DbPool>,
        Path(id): Path<i32>,
    ) -> Result<impl IntoResponse, StatusCode> {
        let response = Json(
            CustomerService::get_customer(&pool, id)
                .await
                .map_err(|e| {
                    warn!("{e}");
                    StatusCode::NOT_FOUND
                })?,
        );
        Ok(response)
    }

    pub async fn get_all_customers(
        State(pool): State<DbPool>,
    ) -> Result<impl IntoResponse, StatusCode> {
        let response = Json(
            CustomerService::get_all_customers(&pool)
                .await
                .map_err(|e| {
                    warn!("{e}");
                    StatusCode::INTERNAL_SERVER_ERROR
                })?,
        );
        Ok(response)
    }
}

use axum::{error_handling::HandleErrorLayer, http::StatusCode, routing::get, BoxError, Router};
use color_eyre::Result;
use std::env;
use std::time::Duration;
use tower::ServiceBuilder;

use crate::{controllers::*, db_actions::get_pool};

pub type DbPool = sqlx::PgPool;
pub struct App;

impl Default for App {
    fn default() -> Self {
        Self::new()
    }
}

impl App {
    pub fn new() -> Self {
        App {}
    }

    pub async fn start_app(self) -> Result<()> {
        let pool = get_pool().await?;
        let router = self.build_router().with_state(pool);
        let addr = env::var("SERVER_ADDR")?;

        axum::Server::bind(&addr.parse()?)
            .serve(router.into_make_service())
            .await?;

        Ok(())
    }

    fn build_router(self) -> Router<DbPool> {
        let api_routes = Router::new().nest("/customer", Routes::customer_routes());

        self.add_error_handler(
            Router::new()
                .route("/", get(Self::hello_world))
                .nest("/api", api_routes),
        )
    }

    async fn hello_world() -> &'static str {
        "Hello, World!"
    }

    fn add_error_handler(self, router: Router<DbPool>) -> Router<DbPool> {
        router.layer(
            ServiceBuilder::new()
                .layer(HandleErrorLayer::new(|error: BoxError| async move {
                    if error.is::<tower::timeout::error::Elapsed>() {
                        Ok(StatusCode::REQUEST_TIMEOUT)
                    } else {
                        Err((
                            StatusCode::INTERNAL_SERVER_ERROR,
                            format!("Unhandled internal error: {}", error),
                        ))
                    }
                }))
                .timeout(Duration::from_secs(10))
                .into_inner(),
        )
    }
}

struct Routes;

impl Routes {
    fn customer_routes() -> Router<DbPool> {
        Router::new()
            .route("/:id", get(CustomerController::get_customer))
            .route("/all", get(CustomerController::get_all_customers))
    }
}

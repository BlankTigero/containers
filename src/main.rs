use color_eyre::Result;
use containers::{db_actions::DbMockData, setup::setup};
use containers::app::App;

async fn fill_db() -> Result<()> {
    let db_mock_data = DbMockData::new();
    db_mock_data.clear().await?;
    db_mock_data.fill().await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    setup().await?;
    fill_db().await?;
    App::new().start_app().await
}

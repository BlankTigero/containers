use axum::async_trait;
use color_eyre::Result;
use sqlx::postgres::PgPool;
use std::env;

use crate::services::CustomerService;

pub async fn get_pool() -> Result<PgPool> {
    let database_url = env::var("DATABASE_URL")?;
    Ok(PgPool::connect(&database_url).await?)
}

#[async_trait]
pub trait MockFillable {
    async fn fill_with_mocked_data(&self) -> Result<()>;
}

#[async_trait]
pub trait Clearable {
    async fn clear(&self) -> Result<()>;
}

pub struct DbMockData {
    pub customer_service: CustomerService,
}

impl Default for DbMockData {
    fn default() -> Self {
        Self::new()
    }
}

impl DbMockData {
    pub fn new() -> Self {
        DbMockData {
            customer_service: CustomerService {},
        }
    }

    pub async fn fill(&self) -> Result<()> {
        self.customer_service.fill_with_mocked_data().await?;
        Ok(())
    }

    pub async fn clear(&self) -> Result<()> {
        self.customer_service.clear().await?;
        Ok(())
    }
}
